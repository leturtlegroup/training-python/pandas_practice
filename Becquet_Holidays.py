import pandas as pd
import numpy as np


df_col = pd.read_csv('colombia.csv')
df_col=df_col.assign(Country="Colombia")
df_cur = pd.read_csv('Curacao.csv')
df_cur=df_cur.assign(Country="Curaçao")
df_total = pd.concat([df_col, df_cur])
print(df_total)

print(pd.merge(df_cur, df_col, how = "inner", on =["DayMonth"]))
df_outer = pd.merge(df_cur, df_col, how = "outer", on =["DayMonth"])
df_comunt  = df_outer["Description_y"] != df_outer["Description_x"]
df_notcommon = df_outer[df_comunt]
print(df_notcommon)
