import pandas as pd
import numpy as np


def _find_unique_holidays(df1, df2, on, filter):
    df_aux = pd.merge(df1, df2, how="left", on=on, suffixes=["", "_c"])
    df_aux = df_aux.where(df_aux[filter+"_c"].isna())[list(df1.columns)].dropna()
    return df_aux


if __name__ == "__main__":
    df_col = pd.read_csv("data/Colombia.csv")
    df_col = df_col.assign(Country="Colombia")
    df_cur = pd.read_csv("data/Curacao.csv")
    df_cur = df_cur.assign(Country="Curaçao")
    df_total = pd.concat([df_col, df_cur])
    print(df_total)

    df_common = df_col.merge(df_cur, how="inner", on=["DayMonth"], suffixes=["_col", "_cur"])
    print(df_common)
    
    df_o_cur = _find_unique_holidays(df_cur, df_col, on="DayMonth", filter="Description")
    df_o_col = _find_unique_holidays(df_col, df_cur, on="DayMonth", filter="Description")

    print(df_o_col)
    print(df_o_cur)