import pandas as pd
from datetime import date, timedelta

def _filder_by_year(df, column ,years: int):
    value_to_check = pd.Timestamp(date.today() - timedelta(days=years * 365))
    filter_mask = df[column] < str(value_to_check)
    return df[filter_mask]


if __name__ == "__main__":
    df = pd.read_csv("data/channels.csv")
    # make sure that my df only contains unique channels
    df_unique = df.drop_duplicates(subset=["channel_id"])

    # Cuantos canales hay
    canales = df_unique.size/len(df_unique.columns)
    canales = df_unique.shape[0]
    canales = len(df_unique)

    # cuantos canales con > 1, 5, 10, 20 millones de subs
    df_1m = df_unique[df_unique["followers"] > 1000000]

    df_5m = df_unique[df_unique["followers"] > 5000000]

    df_10m = df_unique[df_unique["followers"] > 10000000]

    df_20m = df_unique[df_unique["followers"] > 20000000]

    # cuantos canales hay por categoria
    for category, df_category in df_unique.groupby(["category_name"]):
        print(f"My category is '{category}' and the ammount of channels is {len(df_category)}")

    # Distribución por paises
    for category, df_category in df_unique.groupby(["country"]):
        print(f"My country is '{category}' and the ammount of channels is {len(df_category)}")

    df_1y = _filder_by_year(df_unique, "join_date", 1)

    df_2y = _filder_by_year(df_unique, "join_date", 2)

    df_3y = _filder_by_year(df_unique, "join_date", 3)

    df_5y = _filder_by_year(df_unique, "join_date", 5)

    df_desc = df_unique["description"].replace('[\n\r.,]{1}', " ", regex=True).replace('[ ]+', " ", regex=True)
    df_desc = df_desc.description.str.split(" ").explode()
